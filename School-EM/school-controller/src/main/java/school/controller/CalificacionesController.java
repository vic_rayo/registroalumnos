package school.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import school.form.DeleteRatingForm;
import school.form.SaveRatingForm;
import school.generic.ResponseObject;
import school.service.ServiceRating;

@RestController
public class CalificacionesController {
	
	@Autowired
	private ServiceRating serviceRating;
	
	@RequestMapping(value = "/calificarAlumno", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject saveRatingByStudentAndCourse(@RequestBody SaveRatingForm ratingForm) {
		try {
			return new ResponseObject(serviceRating.saveRatingByCourseAndStudent(ratingForm.getMatricula(), ratingForm.getMateria(), ratingForm.getCalificacion()), HttpStatus.CREATED.value(), "EL ALUMNO FUE CALIFICADO CORRECTAMENTE");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
		}
	}
	
	@RequestMapping(value = "/actualizarCalificacionAlumno", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject updateRatingByStudentAndCourse(@RequestBody SaveRatingForm ratingForm) {
		try {
			return new ResponseObject(serviceRating.saveRatingByCourseAndStudent(ratingForm.getMatricula(), ratingForm.getMateria(), ratingForm.getCalificacion()), HttpStatus.CREATED.value(), "LA CALIFICACION DEL ALUMNO FUE MODIFICADA CORRECTAMENTE");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
		}
	}
	
	@RequestMapping(value = "/eliminarCalificacionAlumno", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject deleteRatingByStudentAndCourse(@RequestBody DeleteRatingForm deleteRatingForm) {
		try {
			return new ResponseObject(serviceRating.deleteRatingByCourseAndStudent(deleteRatingForm.getMatricula(), deleteRatingForm.getMateria()), HttpStatus.CREATED.value(), "LA CALIFICACION DEL ALUMNO FUE ELIMINADA CORRECTAMENTE");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
		}
	}

}
