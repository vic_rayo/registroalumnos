package school.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import school.form.GetStudentForm;
import school.form.SaveStudentForm;
import school.generic.ResponseObject;
import school.service.ServiceStudent;

@RestController
public class AlumnosController {
	
	private static final Logger LOG = Logger.getLogger(AlumnosController.class.getName());
	
	@Autowired
	private ServiceStudent serviceStudent;
	
	@RequestMapping(value = "/registrarAlumno", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject saveStudent(@RequestBody SaveStudentForm student) {
		try {
			
			Long matricula = serviceStudent.save(student).getId();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("matricula", matricula);
			return new ResponseObject(map, HttpStatus.CREATED.value(), "EL ALUMNO FUE REGISTRADO CORRECTAMENTE");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
		}
	}
	
	
	@RequestMapping(value = "/consultarAlumnos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject findStudent() {
		try {
			return new ResponseObject(serviceStudent.findAll(), HttpStatus.OK.value(), "LA PETICION FUE REALIZADA CORRECTAMENTE");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), "OCURRIO UN ERROR AL INTENTAR OBTENER A LOS ALUMNOS REGISTRADOS, CONSULTE CON EL ADMINISTRADOR");
		}
	}
	
	@RequestMapping(value = "/consultarAlumnosCalificacion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject findStudentWithRatings() {
		try {
			return new ResponseObject(serviceStudent.findAllWithRatings(), HttpStatus.OK.value(), "LA PETICION FUE REALIZADA CORRECTAMENTE");
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObject(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), "OCURRIO UN ERROR AL INTENTAR OBTENER A LOS ALUMNOS REGISTRADOS, CONSULTE CON EL ADMINISTRADOR");
		}
	}
	

}
