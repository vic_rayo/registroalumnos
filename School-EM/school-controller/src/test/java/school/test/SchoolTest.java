package school.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import school.SchoolApplication;
import school.form.DeleteRatingForm;
import school.form.GetStudentForm;
import school.form.SaveRatingForm;
import school.form.SaveStudentForm;
import school.generic.ResponseObject;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SchoolApplication.class)
@WebAppConfiguration
public class SchoolTest {

	protected MockMvc mvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	private String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	private <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

//	@Test
//	public void saveStudent() throws Exception {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//		String uri = "/registrarAlumno";
//		Set<String> materias = new HashSet<String>();
//		materias.add("Español");
//		materias.add("Matemáticas");
//		materias.add("Química");
//		SaveStudentForm saveStudentForm = new SaveStudentForm("Victor", "Rayón", "Martínez", materias);
//		String inputJson = mapToJson(saveStudentForm);
//		MvcResult mvcResult = mvc.perform(
//				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
//				.andReturn();
//
//		int status = mvcResult.getResponse().getStatus();
//		assertEquals(200, status);
//	}
	
//	@Test
//	public void findAllStudents() throws Exception {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//		String uri = "/consultarAlumnos";
//		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
//		      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
//		 int status = mvcResult.getResponse().getStatus();
//		 assertEquals(200, status);
//	}
	
//	@Test
//	public void findAllStudentsWithRatings() throws Exception {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//		String uri = "/consultarAlumnosCalificacion";
//		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
//		      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
//		 int status = mvcResult.getResponse().getStatus();
//		 assertEquals(200, status);
//	}
	
//	@Test
//	public void ratingStudent() throws Exception {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//		String uri = "/calificarAlumno";
//		SaveRatingForm saveRatingForm = new SaveRatingForm(new Long(1), "Español", 5.6);
//		String inputJson = mapToJson(saveRatingForm);
//		MvcResult mvcResult = mvc.perform(
//				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
//				.andReturn();
//
//		int status = mvcResult.getResponse().getStatus();
//		assertEquals(200, status);
//	}
	
//	@Test
//	public void updateRatingStudent() throws Exception {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//		String uri = "/actualizarCalificacionAlumno";
//		SaveRatingForm saveRatingForm = new SaveRatingForm(new Long(1), "Español", 10.0);
//		String inputJson = mapToJson(saveRatingForm);
//		MvcResult mvcResult = mvc.perform(
//				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
//				.andReturn();
//
//		int status = mvcResult.getResponse().getStatus();
//		assertEquals(200, status);
//	}
	
//	@Test
//	public void deleteRatingStudent() throws Exception {
//		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//		String uri = "/eliminarCalificacionAlumno";
//		DeleteRatingForm deleteRatingForm  = new DeleteRatingForm(new Long(1), "Español");
//		String inputJson = mapToJson(deleteRatingForm);
//		MvcResult mvcResult = mvc.perform(
//				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
//				.andReturn();
//
//		int status = mvcResult.getResponse().getStatus();
//		assertEquals(200, status);
//	}


}
