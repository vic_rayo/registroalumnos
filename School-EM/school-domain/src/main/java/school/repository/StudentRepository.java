package school.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import school.entity.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long>{
	
	public Student save(Student student);
	public List<Student> findAll();
	public Optional<Student> findById(Long id);

}
