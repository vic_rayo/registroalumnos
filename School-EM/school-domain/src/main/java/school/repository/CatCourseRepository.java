package school.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import school.entity.CatCourse;

@Repository
public interface CatCourseRepository extends CrudRepository<CatCourse, Integer>{
	
	public List<CatCourse> findAll();
}
