package school.repository;

import org.springframework.data.repository.CrudRepository;

import school.entity.StudentCourse;

public interface StudentCourseRepository extends CrudRepository<StudentCourse, Integer>{
	
	public StudentCourse save(StudentCourse studentCourse);
	
}
