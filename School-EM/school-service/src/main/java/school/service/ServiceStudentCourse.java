package school.service;

import school.entity.StudentCourse;

public interface ServiceStudentCourse {
	
	public StudentCourse save(StudentCourse studentCourse);

}
