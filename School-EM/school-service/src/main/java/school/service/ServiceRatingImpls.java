package school.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import school.entity.Student;
import school.entity.StudentCourse;


@Service
public class ServiceRatingImpls implements ServiceRating{
	
	@Autowired
	private ServiceStudent serviceStudent;
	
	@Autowired
	private ServiceStudentCourse serviceStudentCourse;

	@Override
	public boolean saveRatingByCourseAndStudent(Long matricula, String course, Double rating) throws Exception{
		Student student = serviceStudent.findById(matricula);
		List<StudentCourse> listaMaterias = student.getListCourse();
		StudentCourse studentCourseSaved = null;
		
		
		for (StudentCourse studentCourse : listaMaterias) {
			if(course.equals(studentCourse.getCatCourse().getDescripcion())) {
				studentCourse.setCalificacion(rating);
				studentCourseSaved = serviceStudentCourse.save(studentCourse);
			}
		}
		
		if(studentCourseSaved == null) {
			throw new Exception("EL ALUMNO CON LA MATRICULA PROPORCIONADA NO CUENTA CON ESA MATERIA INSCRITA");
		}else if(studentCourseSaved.getCalificacion() == rating) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean deleteRatingByCourseAndStudent(Long matricula, String course) throws Exception {
		return saveRatingByCourseAndStudent(matricula, course, null);
	}

}
