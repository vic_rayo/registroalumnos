package school.service;

import java.util.List;

import school.entity.Student;
import school.form.GetStudentForm;
import school.form.SaveStudentForm;

public interface ServiceStudent{

	public Student save(SaveStudentForm student) throws Exception;
	public List<GetStudentForm> findAll();
	public Student findById(Long id);
	public List<GetStudentForm> findAllWithRatings();
}
