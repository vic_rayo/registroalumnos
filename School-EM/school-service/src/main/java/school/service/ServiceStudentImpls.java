package school.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import school.entity.CatCourse;
import school.entity.Student;
import school.entity.StudentCourse;
import school.form.GetStudentForm;
import school.form.SaveStudentForm;
import school.repository.StudentRepository;

@Service
public class ServiceStudentImpls implements ServiceStudent{
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private ServiceCatCourse serviceCatCourse;

	@Override
	public Student save(SaveStudentForm studentParam) throws Exception{
		Student student = new Student();
		student.setPaterno(studentParam.getPaterno());
		student.setMaterno(studentParam.getMaterno());
		student.setNombre(studentParam.getNombre());
		
		Set<String> materias =  studentParam.getMaterias();
		
		if(materias == null || materias.size() == 0) {
			throw new Exception("PARA REGISTRAR UN ESTUDIANTE DEBE TENER MATERIAS ASIGNADAS");
		}
		
		List<CatCourse> catalogoMaterias = serviceCatCourse.findAll();
		List<StudentCourse> listaMaterias = new ArrayList<StudentCourse>();
		
		for (String materia : materias) {
			
			for (CatCourse catCourse : catalogoMaterias) {
				if(materia.equals(catCourse.getDescripcion())) {
					StudentCourse studentCourse = new StudentCourse();
					studentCourse.setCatCourse(catCourse);
					studentCourse.setStudent(student);
					listaMaterias.add(studentCourse);
				}
			}
			
		}
		
		if(listaMaterias.size() == 0) {
			throw new Exception("LAS MATERIAS HABILITADAS PARA ASGINAR SON: Español, Matemáticas y Química");
		}
		
		student.setListCourse(listaMaterias);
		return studentRepository.save(student);
	}

	@Override
	public List<GetStudentForm> findAll() {
		List<Student> lista = studentRepository.findAll();
		return armaListado(lista, false);
	}

	@Override
	public Student findById(Long id) {
		Student student = studentRepository.findById(id).get();
		return student;
	}

	@Override
	public List<GetStudentForm> findAllWithRatings() {
		List<Student> lista = studentRepository.findAll();
		return armaListado(lista, true);
	}
	
	public List<GetStudentForm> armaListado(List<Student> lista, boolean conCalificaciones){
		List<GetStudentForm> listaForm = new ArrayList<GetStudentForm>();
			List<String> listaMaterias = new ArrayList<String>();
		
		for (Student student : lista) {
			listaMaterias.clear();
			GetStudentForm studentForm = new GetStudentForm();
			for (StudentCourse studentCourse : student.getListCourse()) {
				if(conCalificaciones) {
					if(studentCourse.getCalificacion() != null) {
						listaMaterias.add(studentCourse.getCatCourse().getDescripcion() + " : " + studentCourse.getCalificacion().toString());
					}else {
						listaMaterias.add(studentCourse.getCatCourse().getDescripcion() + " : Sin Calificación");
					}
					
				}else {
					listaMaterias.add(studentCourse.getCatCourse().getDescripcion());
				}
				
			}
			Set<String> set = new HashSet<String>(listaMaterias);
			studentForm.setMaterias(set);
			studentForm.setNombreCompleto(student.getNombre() + " " + student.getPaterno() + " " + student.getMaterno());
			studentForm.setMatricula(student.getId());
			listaForm.add(studentForm);
		}
		
		return listaForm;
	}

}
