package school.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import school.entity.CatCourse;
import school.repository.CatCourseRepository;

@Service
public class ServiceCatCourseImpls implements ServiceCatCourse{
	
	@Autowired
	private CatCourseRepository catCourseRepository;

	@Override
	public List<CatCourse> findAll() {
		return catCourseRepository.findAll();
	}

}
