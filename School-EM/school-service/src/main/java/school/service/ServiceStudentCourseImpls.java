package school.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import school.entity.StudentCourse;
import school.repository.StudentCourseRepository;

@Service
public class ServiceStudentCourseImpls implements ServiceStudentCourse{
	
	@Autowired
	private StudentCourseRepository studentCourseRepository;

	@Override
	public StudentCourse save(StudentCourse studentCourse) {
		return studentCourseRepository.save(studentCourse);
	}

}
