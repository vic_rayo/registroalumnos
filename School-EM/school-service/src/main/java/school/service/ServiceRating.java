package school.service;

public interface ServiceRating {
	
	public boolean saveRatingByCourseAndStudent(Long matricula, String course, Double rating) throws Exception;
	public boolean deleteRatingByCourseAndStudent(Long matricula, String course) throws Exception;

}
