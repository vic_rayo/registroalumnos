package school.service;

import java.util.List;

import school.entity.CatCourse;

public interface ServiceCatCourse {

	public List<CatCourse> findAll();
}
