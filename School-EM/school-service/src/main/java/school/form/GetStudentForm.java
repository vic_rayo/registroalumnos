package school.form;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GetStudentForm {
	
	@NonNull
	private Long matricula;
	
	@NonNull
	private String nombreCompleto;
	
	@NonNull
	private Set<String> materias;
	
}
