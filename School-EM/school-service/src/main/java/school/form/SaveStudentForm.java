package school.form;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SaveStudentForm {
	
	@NonNull
	private String nombre;
	
	@NonNull
	private String paterno;
	
	@NonNull
	private String materno;
	
	@NonNull
	private Set<String> materias;
}
